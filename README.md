# DevOps QuickSurvive

## GetStarted
### Fork project
A fork is a copy of a project. Forking a repository allows you to make changes without affecting the original project.
1. Go to DevOps-QuickSurvive repository: https://gitlab.com/suprman.s/devops-quicksurvive
2. At the right hand side of page, there's a fork symbol (close to star symbol). Chick that icon!
3. Naming as you want, select project URL including project slug, and select visibility level.
4. Click **Fork project**
5. Go to your project

### Clone your project to your computer
1. Open VSCode
2. Open your working directory in VSCode
3. In VSCode menu, click `Terminal` > `New Terminal`
4. Using following command to clone code from your forked repository at URL: [Your_GitLab_URL]

        git clone [Your_GitLab_URL]

    For example, `git clone https://gitlab.com/suprman.s/devops-quicksurvive.git`

### Start React Web in your computer
1. Go to working folder:

        cd devops-quicksurvive/itscool

2. Install package using `npm`

        npm install

3. After finish install all packages, there's a folder `node_modules` and run to start development server

        npm start

4. Go to development website: http://localhost:3000

### Deployment variables
Before running deployment stage, you have to configure those following variables to your GitLab repository. Click `Settings` > `CI/CD` > `Variables` > `Add variable`.
- `$PROD_SERVER_IP`, your public access IP address via SSH
- `$PROD_SERVER_PORT`, your public access IP port of SSH
- `$SSH_PRIVATE_KEY`, as file and you have to generate in destination server
- `$IMAGE_URL`, you can found in `Deploy` > `Container Registry` with commonly: `registry.gitlab.com/<username>/<image-name-with-tag>`

## Learn React.js *Free* from YouTube
This React.js website is part of the **React Course by freeCodeCamp**  and has been revised to be compatible with this DevOps Series.

If you want to learn more for *FREE!*, check out 
[React Course - Beginner's Tutorial for React JavaScript Library [2022]](https://youtu.be/bMknfKXIFA8?si=_hD6k09XlMIZB_Y7). Teaching by Bob Ziroll, Head of Education at Scrimba.


## For more information
For more information or contact me, please checkout: https://linktr.ee/suprman.s


Shalong Samretngan \
Jan 2024
#