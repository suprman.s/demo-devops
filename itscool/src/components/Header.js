import React from "react";
import "./Header.css"
import TrollFaceIMG from "./images/troll-face.png" 

export default function Header() {
    return (
        <header className="header">
            <img src={TrollFaceIMG} alt="" className="header--image"></img>
            <h2 className="header--title">Meeeeemmmmme</h2>

            {/* <h4 className="header--project">it's cool</h4> */}
            {/* error  `'` can be escaped with `&apos;`, `&lsquo;`, `&#39;`, `&rsquo;`  react/no-unescaped-entities */}

            <h4 className="header--project">it&apos;s cool</h4>
        </header>
    )
}