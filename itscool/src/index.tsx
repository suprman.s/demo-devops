import React  from "react"
import App from "./App";
import { createRoot } from 'react-dom/client'

// const container = document.getElementById('root');
// const root = createRoot(container!); // createRoot(container!) if you use TypeScript (.tsx)
// root.render(<App />);

// warning  Forbidden non-null assertion  @typescript-eslint/no-non-null-assertion

const container = document.getElementById('root');

if (container) {
  const root = createRoot(container);
  root.render(<App />);
} else {
  console.error("Container element with id 'root' not found.");
}